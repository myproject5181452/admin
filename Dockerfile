FROM golang:1.21.5-bullseye 

WORKDIR /usr/src/admin

COPY . .

RUN go mod download

# Build
RUN go build -o /main ./cmd/main.go

ARG PROXY

ENV HTTP_PROXY=${PROXY}
ENV HTTPS_PROXY=${PROXY}
ENV NO_PROXY="driver:9094"


RUN export http_proxy=${PROXY} \
    && export https_proxy=${PROXY} \
    && apt-get update -y
    # && go test -v ./...

EXPOSE 9091
CMD [ "/main" ]