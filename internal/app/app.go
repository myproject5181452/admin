package app

import (
	"log"
	"net"
	"os"

	"github.com/akijura/myProject/admin/internal/config"
	"github.com/akijura/myProject/admin/internal/controller"
	pb "github.com/akijura/myProject/admin/internal/controller/pb"
	"github.com/akijura/myProject/admin/internal/repository"
	"github.com/akijura/myProject/admin/internal/service"
	"github.com/akijura/myProject/admin/pkg/postgresx"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

var RootPath, _ = os.Getwd()

func Run() {
	zap.L().Info("initializing zap logger")
	logger := setUpLogger()
	defer logger.Sync()
	zap.ReplaceGlobals(logger)

	// initializing configs
	zap.L().Info("initializing configs")
	cfg := config.NewConfig()
	// connecting to database
	dbx, err := postgresx.New(cfg.DBUrl, postgresx.MaxConnAttempts(15), postgresx.MaxPoolSize(10))
	if err != nil {
		panic(err)
	}
	defer dbx.Close()
	// initializing repositories
	zap.L().Info("initializing repositories")
	repo := repository.NewRepositories(dbx)

	// initializing services
	zap.L().Info("initializing services")
	serv := service.NewService(repo, cfg)

	// initializing controllers
	zap.L().Info("initializing controllers")
	ctrl := controller.NewController(serv, repo, cfg)

	zap.L().Info("Starting  grpc service...")
	lis, err := net.Listen("tcp", cfg.ServerRunPort)
	if err != nil {
		log.Fatal(err)
	}

	s := grpc.NewServer()
	pb.RegisterAdminSerServer(s, ctrl)
	zap.L().Info("server listening at " + lis.Addr().String())
	if err := s.Serve(lis); err != nil {
		zap.L().Error("failed to serve: " + err.Error())
	}
}
