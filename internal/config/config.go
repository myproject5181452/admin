package config

import (
	"fmt"
	"os"
)

type Config struct {
	ServerRunPort string
	JwtSignKey    string
	PasswordSalt  string

	DBUrl string

	FileLocation string
}

func NewConfig() *Config {
	cfg := &Config{}

	//err := godotenv.Load(testRun())

	name := os.Getenv("POSTGRES_DB")
	user := os.Getenv("POSTGRES_USER")
	pass := os.Getenv("POSTGRES_PASSWORD")
	port := os.Getenv("POSTGRES_PORT")
	host := os.Getenv("POSTGRES_HOST")

	cfg.JwtSignKey = os.Getenv("JWT_SIGN_KEY")
	cfg.PasswordSalt = os.Getenv("PASSWORD_SALT")
	cfg.ServerRunPort = os.Getenv("SERVER_RUN_PORT")

	cfg.FileLocation = os.Getenv("FILE_LOCATION")

	cfg.DBUrl = fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", user, pass, host, port, name)

	return cfg
}
