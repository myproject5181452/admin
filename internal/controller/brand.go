package controller

import (
	"context"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (c *Controller) CreateBrand(ctx context.Context, req *pb.CreateBrandRequest) (*pb.CreateBrandResponse, error) {
	var createBrand domain.BrandCreateInput
	createBrand.Name = req.GetName()
	createBrand.Status = int(req.GetStatus())
	createBrand.AuthorID = req.AuthorId

	err := c.s.Brand.CreateBrand(ctx, createBrand)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.CreateBrandResponse{
		Status: "successfuly created",
	}
	return rsp, nil
}

func (c *Controller) GetAllBrands(ctx context.Context, req *pb.GetAllBrandsRequest) (*pb.GetAllBrandsResponse, error) {
	brands, err := c.s.Brand.GetAllBrands(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	var res []*pb.Brand
	for _, brand := range brands {
		res = append(res, dbmodel.BrandToProto(brand))
	}
	rsp := &pb.GetAllBrandsResponse{
		Brands: res,
	}
	return rsp, nil
}

func (c *Controller) GetBrandByID(ctx context.Context, req *pb.GetBrandByIDRequest) (*pb.GetBrandByIDResponse, error) {
	brand, err := c.s.Brand.GetBrandByID(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.GetBrandByIDResponse{
		Brand: dbmodel.BrandToProto(brand),
	}
	return rsp, nil
}

func (c *Controller) UpdateBrand(ctx context.Context, req *pb.UpdateBrandRequest) (*pb.UpdateBrandResponse, error) {
	var updateBrand domain.BrandUpdateInput
	updateBrand.ID = req.GetID()
	updateBrand.Name = req.GetName()
	updateBrand.Status = int(req.Status)
	updateBrand.AuthorID = int(req.GetAuthorId())

	err := c.s.Brand.UpdateBrand(ctx, updateBrand)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.UpdateBrandResponse{
		Status: "Successfuly updated",
	}
	return rsp, nil
}

func (c *Controller) DeleteBrand(ctx context.Context, req *pb.DeleteBrandRequest) (*pb.DeleteBrandResponse, error) {
	err := c.s.Brand.DeleteBrand(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.DeleteBrandResponse{
		Status: "Successfuly deleted",
	}
	return rsp, nil
}
