package controller

import (
	"context"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (c *Controller) CreateCarInfo(ctx context.Context, req *pb.CreateCarInfoRequest) (*pb.CreateCarInfoResponse, error) {
	var createCarInfo domain.CarInfoCreateInput
	createCarInfo.ModelId = req.GetModelId()
	createCarInfo.Length = req.GetLength()
	createCarInfo.Width = req.GetWidth()
	createCarInfo.Height = req.GetHeight()
	createCarInfo.Volume = req.GetVolume()
	createCarInfo.LoadCapacity = req.GetLoadCapacity()
	createCarInfo.Close = req.GetClose()
	createCarInfo.Description = req.GetDescription()
	createCarInfo.AuthorID = req.GetAuthorId()

	err := c.s.CarInfo.CreateCarInfo(ctx, createCarInfo)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.CreateCarInfoResponse{
		Code: pb.StatusCode_Ok,
	}
	return rsp, nil
}

func (c *Controller) GetAllCarInfos(ctx context.Context, req *pb.GetAllCarInfosRequest) (*pb.GetAllCarInfosResponse, error) {
	carInfos, err := c.s.CarInfo.GetAllCarInfos(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	var res []*pb.CarInfo
	for _, carInfo := range carInfos {
		res = append(res, dbmodel.CarInfoToProto(carInfo))
	}
	rsp := &pb.GetAllCarInfosResponse{
		CarInfos: res,
	}
	return rsp, nil
}

func (c *Controller) GetCarInfoByID(ctx context.Context, req *pb.GetCarInfoByIDRequest) (*pb.GetCarInfoByIDResponse, error) {
	carInfo, err := c.s.CarInfo.GetCarInfoByID(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.GetCarInfoByIDResponse{
		CarInfo: dbmodel.CarInfoToProto(carInfo),
	}
	return rsp, nil
}
func (c *Controller) GetAllCarInfosByModelID(ctx context.Context, req *pb.GetAllCarInfoByModelRequest) (*pb.GetAllCarInfoByModelResponse, error) {
	carInfos, err := c.s.CarInfo.GetAllCarInfosByModelID(ctx, req.GetModelId())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	var res []*pb.CarInfo
	for _, carInfo := range carInfos {
		res = append(res, dbmodel.CarInfoToProto(carInfo))
	}
	rsp := &pb.GetAllCarInfoByModelResponse{
		CarInfos: res,
	}
	return rsp, nil
}
func (c *Controller) UpdateCarInfo(ctx context.Context, req *pb.UpdateCarInfoRequest) (*pb.UpdateCarInfoResponse, error) {
	var updateCarInfo domain.CarInfoUpdateInput
	updateCarInfo.ID = req.GetID()
	updateCarInfo.ModelID = req.GetModelId()
	updateCarInfo.Length = req.GetLength()
	updateCarInfo.Width = req.GetWidth()
	updateCarInfo.Height = req.GetHeight()
	updateCarInfo.Volume = req.GetVolume()
	updateCarInfo.LoadCapacity = req.GetLoadCapacity()
	updateCarInfo.Close = req.GetClose()
	updateCarInfo.Description = req.GetDescription()
	updateCarInfo.AuthorID = req.GetAuthorId()

	err := c.s.CarInfo.UpdateCarInfo(ctx, updateCarInfo)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.UpdateCarInfoResponse{
		Code: 1,
	}
	return rsp, nil
}
func (c *Controller) DeleteCarInfo(ctx context.Context, req *pb.DeleteCarInfoRequest) (*pb.DeleteCarInfoResponse, error) {
	err := c.s.CarInfo.DeleteCarInfo(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.DeleteCarInfoResponse{
		Code: 1,
	}
	return rsp, nil
}
