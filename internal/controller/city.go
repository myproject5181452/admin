package controller

import (
	"context"
	"strconv"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (c *Controller) CreateCity(ctx context.Context, req *pb.CreateCityRequest) (*pb.CreateCityResponse, error) {
	var createCity domain.CityCreateInput
	createCity.Name = req.GetName()
	createCity.Status, _ = strconv.Atoi(req.GetStatus())
	createCity.CountryID = req.GetCountryId()
	createCity.AuthorID = req.AuthorId

	err := c.s.City.CreateCity(ctx, createCity)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.CreateCityResponse{
		Status: "successfuly created",
	}
	return rsp, nil
}

func (c *Controller) GetAllCities(ctx context.Context, req *pb.GetAllCitiesRequest) (*pb.GetAllCitiesResponse, error) {
	cities, err := c.s.City.GetAllCities(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	var res []*pb.City
	for _, city := range cities {
		res = append(res, dbmodel.CityToProto(city))
	}
	rsp := &pb.GetAllCitiesResponse{
		Cities: res,
	}
	return rsp, nil
}

func (c *Controller) GetAllCitiesByCountryID(ctx context.Context, req *pb.GetAllCitiesByCountryRequest) (*pb.GetAllCitiesByCountryResponse, error) {
	cities, err := c.s.City.GetAllCitiesByCountryID(ctx, req.GetCountryId())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	var res []*pb.City
	for _, city := range cities {
		res = append(res, dbmodel.CityToProto(city))
	}
	rsp := &pb.GetAllCitiesByCountryResponse{
		Cities: res,
	}
	return rsp, nil
}

func (c *Controller) GetCityByID(ctx context.Context, req *pb.GetCityByIDRequest) (*pb.GetCityByIDResponse, error) {
	city, err := c.s.City.GetCityByID(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.GetCityByIDResponse{
		City: dbmodel.CityToProto(city),
	}
	return rsp, nil
}
func (c *Controller) UpdateCity(ctx context.Context, req *pb.UpdateCityRequest) (*pb.UpdateCityResponse, error) {
	var updateCity domain.CityUpdateInput
	updateCity.ID = req.GetID()
	updateCity.Name = req.GetName()
	updateCity.Status, _ = strconv.Atoi(req.GetStatus())
	updateCity.CountryID = req.GetCountryId()
	updateCity.AuthorID = int(req.GetAuthorId())

	err := c.s.City.UpdateCity(ctx, updateCity)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.UpdateCityResponse{
		Status: "Successfuly updated",
	}
	return rsp, nil
}

func (c *Controller) DeleteCity(ctx context.Context, req *pb.DeleteCityRequest) (*pb.DeleteCityResponse, error) {
	err := c.s.City.DeleteCity(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.DeleteCityResponse{
		Status: "Successfuly deleted",
	}
	return rsp, nil
}
