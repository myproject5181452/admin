package controller

import (
	"github.com/akijura/myProject/admin/internal/config"
	pb "github.com/akijura/myProject/admin/internal/controller/pb"
	"github.com/akijura/myProject/admin/internal/repository"
	"github.com/akijura/myProject/admin/internal/service"
)

type Controller struct {
	s    *service.Service
	repo *repository.Repositories
	conf *config.Config
	pb.UnimplementedAdminSerServer
}

func NewController(s *service.Service, repo *repository.Repositories, cfg *config.Config) *Controller {
	return &Controller{s: s, conf: cfg, repo: repo}
}
