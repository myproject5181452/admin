package controller

import (
	"context"
	"strconv"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (c *Controller) CreateCountry(ctx context.Context, req *pb.CreateCountryRequest) (*pb.CreateCountryResponse, error) {
	var createCountry domain.CountryCreateInput
	createCountry.Name = req.GetName()
	createCountry.ShortName = req.GetShortName()
	createCountry.Status, _ = strconv.Atoi(req.GetStatus())
	createCountry.AuthorID = int(req.GetAuthorId())

	err := c.s.Country.CreateCountry(ctx, createCountry)
	if err != nil {
		return nil, status.Error(codes.Internal, ErrCantCreateCountry.Error())
	}
	rsp := &pb.CreateCountryResponse{
		Status: "successfuly created",
	}
	return rsp, nil
}

func (c *Controller) GetAllCountry(ctx context.Context, req *pb.GetAllCountryRequest) (*pb.GetAllCountryResponse, error) {
	countries, err := c.s.Country.GetAllCountry(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, ErrCantGetAllCountry.Error())
	}
	var res []*pb.Country
	for _, country := range countries {
		res = append(res, dbmodel.CountryToProto(country))
	}
	rsp := &pb.GetAllCountryResponse{
		Countries: res,
	}
	return rsp, nil
}

func (c *Controller) GetCountryByID(ctx context.Context, req *pb.GetCountryByIDRequest) (*pb.GetCountryByIDResponse, error) {
	country, err := c.s.Country.GetCountryByID(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.GetCountryByIDResponse{
		Country: dbmodel.CountryToProto(country),
	}
	return rsp, nil
}

func (c *Controller) UpdateCountry(ctx context.Context, req *pb.UpdateCountryRequest) (*pb.UpdateCountryResponse, error) {
	var updateCountry domain.CountryUpdateInput
	updateCountry.ID = req.GetID()
	updateCountry.Name = req.GetName()
	updateCountry.ShortName = req.GetShortName()
	updateCountry.Status, _ = strconv.Atoi(req.GetStatus())
	updateCountry.AuthorID = int(req.GetAuthorId())

	err := c.s.Country.UpdateCountry(ctx, updateCountry)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.UpdateCountryResponse{
		Status: "Successfuly updated",
	}
	return rsp, nil
}

func (c *Controller) DeleteCountry(ctx context.Context, req *pb.DeleteCountryRequest) (*pb.DeleteCountryResponse, error) {
	err := c.s.Country.DeleteCountry(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.DeleteCountryResponse{
		Status: "Successfuly deleted",
	}
	return rsp, nil
}
