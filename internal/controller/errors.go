package controller

import "errors"

var (
	ErrCantCreateCountry = errors.New("failed to create country")
	ErrCantGetAllCountry = errors.New("failed to get all countries")

	ErrCantCreateUser = errors.New("failed to create user")
)
