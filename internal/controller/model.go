package controller

import (
	"context"
	"strconv"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (c *Controller) CreateModel(ctx context.Context, req *pb.CreateModelRequest) (*pb.CreateModelResponse, error) {
	var createModel domain.ModelCreateInput
	createModel.Name = req.GetName()
	createModel.Status, _ = strconv.Atoi(req.GetStatus())
	createModel.BrandID = req.GetBrandId()
	createModel.AuthorID = req.AuthorId

	err := c.s.Model.CreateModel(ctx, createModel)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.CreateModelResponse{
		Status: "successfuly created",
	}
	return rsp, nil
}

func (c *Controller) GetAllModels(ctx context.Context, req *pb.GetAllModelsRequest) (*pb.GetAllModelsResponse, error) {
	models, err := c.s.Model.GetAllModels(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	var res []*pb.Model
	for _, model := range models {
		res = append(res, dbmodel.ModelToProto(model))
	}
	rsp := &pb.GetAllModelsResponse{
		Models: res,
	}
	return rsp, nil
}

func (c *Controller) GetAllModelsByBrandID(ctx context.Context, req *pb.GetAllModelsByBrandRequest) (*pb.GetAllModelsByBrandResponse, error) {
	models, err := c.s.Model.GetAllModelsByBrandID(ctx, req.GetBrandId())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	var res []*pb.Model
	for _, model := range models {
		res = append(res, dbmodel.ModelToProto(model))
	}
	rsp := &pb.GetAllModelsByBrandResponse{
		Models: res,
	}
	return rsp, nil
}

func (c *Controller) GetModelByID(ctx context.Context, req *pb.GetModelByIDRequest) (*pb.GetModelByIDResponse, error) {
	model, err := c.s.Model.GetModelByID(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.GetModelByIDResponse{
		Model: dbmodel.ModelToProto(model),
	}
	return rsp, nil
}

func (c *Controller) UpdateModel(ctx context.Context, req *pb.UpdateModelRequest) (*pb.UpdateModelResponse, error) {
	var updateModel domain.ModelUpdateInput
	updateModel.ID = req.GetID()
	updateModel.Name = req.GetName()
	updateModel.Status, _ = strconv.Atoi(req.GetStatus())
	updateModel.BrandID = req.GetBrandId()
	updateModel.AuthorID = int(req.GetAuthorId())

	err := c.s.Model.UpdateModel(ctx, updateModel)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.UpdateModelResponse{
		Status: "Successfuly updated",
	}
	return rsp, nil
}

func (c *Controller) DeleteModel(ctx context.Context, req *pb.DeleteModelRequest) (*pb.DeleteModelResponse, error) {
	err := c.s.Model.DeleteModel(ctx, req.GetID())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.DeleteModelResponse{
		Status: "Successfuly deleted",
	}
	return rsp, nil
}
