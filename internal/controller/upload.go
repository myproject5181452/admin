package controller

import (
	"io"
	"log"
	"os"
	"path"
	"strconv"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
	"github.com/pkg/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func (c *Controller) UploadFile(stream pb.AdminSer_UploadFileServer) error {
	// Read metadata from client.
	md, ok := metadata.FromIncomingContext(stream.Context())
	if !ok {
		return status.Errorf(codes.DataLoss, "ClientStreamingEcho: failed to get metadata")
	}
	filename := md["filename"][0]
	filesize, _ := strconv.Atoi(md["size"][0])
	log.Printf("Received Upload name: %v, size: %v", filename, filesize)

	// while there are messages coming
	size := 0
	file, err := os.OpenFile(path.Join(c.conf.FileLocation, filename), os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer file.Close()
	for {
		data, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Println(err)
			return errors.Wrapf(err, "failed unexpectadely while reading chunks from stream")
		}
		size += len(data.Chunk)
		file.Write(data.Chunk)
	}
	statusCode := pb.StatusCode_Ok
	if size != filesize {
		statusCode = pb.StatusCode_Failed
	}
	err = stream.SendAndClose(&pb.FileUploadResponse{
		Code: statusCode,
	})
	return err
	// log.Println("inside controller")
	// file := upload.NewFile()
	// var fileSize uint32
	// fileSize = 0
	// defer func() {
	// 	if err := file.OutputFile.Close(); err != nil {
	// 		log.Println(err)
	// 	}
	// }()
	// for {
	// 	log.Println("inside controller loop")
	// 	req, err := stream.Recv()
	// 	log.Println(c.conf.FileLocation)
	// 	file.SetFile(req.GetFileName(), c.conf.FileLocation)

	// 	if err == io.EOF {
	// 		break
	// 	}
	// 	if err != nil {
	// 		return status.Error(codes.Internal, err.Error())
	// 	}
	// 	chunk := req.GetChunk()
	// 	fileSize += uint32(len(chunk))
	// 	log.Printf("received a chunk with size: %d", fileSize)
	// 	if err := file.Write(chunk); err != nil {
	// 		return status.Error(codes.Internal, err.Error())
	// 	}
	// }
	// log.Println("outside loop in controller")
	// fileName := filepath.Base(file.FilePath)
	// log.Printf("saved file: %s, size: %d", fileName, fileSize)
	// return stream.SendAndClose(&pb.FileUploadResponse{FileName: fileName, Size: fileSize})
}

func (c *Controller) DownloadFile(req *pb.FileDownloadRequest, responseStream pb.AdminSer_DownloadFileServer) error {
	log.Printf("Received Download")
	filePath := path.Join(c.conf.FileLocation, req.GetFileName())
	stats, err := os.Stat(filePath)
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}
	log.Println("checking stats")
	log.Println(stats.Mode())
	header := metadata.New(map[string]string{"filename": req.GetFileName(),
		"size": strconv.Itoa(int(stats.Size()))})
	responseStream.SendHeader(header)
	log.Println("after send header")
	file, err := os.Open(filePath)
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}
	defer file.Close()
	buf := make([]byte, 1024)

	for {
		n, err := file.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return status.Error(codes.Internal, err.Error())
		}
		err = responseStream.Send(&pb.FileDownloadResponse{
			FileChunk: buf[:n],
		})
		if err != nil {
			return status.Error(codes.Internal, err.Error())
		}
	}
	return nil
	// for {
	// 	bytesRead, err := file.Read(buff)
	// 	if err != nil {
	// 		if err != io.EOF {
	// 			fmt.Println(err)
	// 		}
	// 		break
	// 	}
	// 	resp := &pb.FileDownloadResponse{
	// 		FileChunk: buff[:bytesRead],
	// 	}
	// 	err = responseStream.Send(resp)
	// 	if err != nil {
	// 		log.Println("error while sending chunk:", err)
	// 		return err
	// 	}
	// }
	// return nil
}
