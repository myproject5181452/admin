package controller

import (
	"context"
	"strconv"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (c *Controller) CreateUser(ctx context.Context, req *pb.CreateUserRequest) (*pb.CreateUserResponse, error) {
	var createUser domain.UserCreateInput
	createUser.Login = req.GetLogin()
	createUser.Password = req.GetPassword()
	createUser.Name = req.GetName()
	createUser.PhoneNumber = req.GetPhoneNumber()
	createUser.Role = int(req.GetRole())
	createUser.Status, _ = strconv.Atoi(req.GetStatus())

	err := c.s.User.CreateUser(ctx, createUser)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	rsp := &pb.CreateUserResponse{
		Status: "successfuly created",
	}
	return rsp, nil
}
