package dbmodel

import (
	"fmt"
	"time"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
)

type Brand struct {
	ID         int64     `db:"id" json:"id,omitempty"`
	Name       string    `db:"name" json:"name,omitempty"`
	Status     int       `db:"status" json:"status,omitempty"`
	AuthorID   int64     `db:"author_id" json:"author_id,omitempty"`
	AuthorName string    `json:"author_name,omitempty"`
	CreatedAt  time.Time `db:"created_at" json:"created_at,omitempty"`
	UpdatedAt  time.Time `db:"updated_at" json:"updated_at,omitempty"`
}

func BrandToProto(city Brand) *pb.Brand {
	return &pb.Brand{
		ID:         city.ID,
		Name:       city.Name,
		Status:     fmt.Sprint(city.Status),
		AuthorId:   city.AuthorID,
		AuthorName: city.AuthorName,
		CreatedAt:  fmt.Sprint(city.CreatedAt),
		UpdatedAt:  fmt.Sprint(city.UpdatedAt),
	}
}
