package dbmodel

import (
	"fmt"
	"time"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
)

type CarInfo struct {
	ID           int64     `db:"id" json:"id,omitempty"`
	ModelId      int64     `db:"model_id" json:"model_id"`
	Length       int32     `db:"length" json:"length"`
	Width        int32     `db:"width" json:"width"`
	Height       int32     `db:"height" json:"height"`
	Volume       int32     `db:"volume" json:"volume"`
	LoadCapacity int32     `db:"load_capacity" json:"load_capacity"`
	Close        int32     `db:"close" json:"close"`
	Description  string    `db:"decription" json:"decription"`
	AuthorID     int64     `db:"author_id" json:"author_id"`
	AuthorName   string    `json:"author_name,omitempty"`
	ModelName    string    `json:"model_name,omitempty"`
	CreatedAt    time.Time `db:"created_at" json:"created_at,omitempty"`
	UpdatedAt    time.Time `db:"updated_at" json:"updated_at,omitempty"`
}

func CarInfoToProto(carInfo CarInfo) *pb.CarInfo {
	return &pb.CarInfo{
		ID:           carInfo.ID,
		ModelId:      carInfo.ModelId,
		Length:       carInfo.Length,
		Width:        carInfo.Width,
		Height:       carInfo.Height,
		Volume:       carInfo.Volume,
		LoadCapacity: carInfo.LoadCapacity,
		Close:        carInfo.Close,
		Description:  carInfo.Description,
		AuthorId:     carInfo.AuthorID,
		AuthorName:   carInfo.AuthorName,
		ModelName:    carInfo.ModelName,
		CreatedAt:    fmt.Sprint(carInfo.CreatedAt),
		UpdatedAt:    fmt.Sprint(carInfo.UpdatedAt),
	}
}
