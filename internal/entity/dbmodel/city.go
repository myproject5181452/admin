package dbmodel

import (
	"fmt"
	"time"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
)

type City struct {
	ID          int64     `db:"id" json:"id,omitempty"`
	Name        string    `db:"name" json:"name,omitempty"`
	Status      int       `db:"status" json:"status,omitempty"`
	CountryID   int64     `db:"country_id" json:"country_id,omitempty"`
	AuthorID    int64     `db:"author_id" json:"author_id,omitempty"`
	CountryName string    `json:"country_name,omitempty"`
	AuthorName  string    `json:"author_name,omitempty"`
	CreatedAt   time.Time `db:"created_at" json:"created_at,omitempty"`
	UpdatedAt   time.Time `db:"updated_at" json:"updated_at,omitempty"`
}

func CityToProto(city City) *pb.City {
	return &pb.City{
		ID:          city.ID,
		Name:        city.Name,
		Status:      fmt.Sprint(city.Status),
		CountryId:   city.CountryID,
		AuthorId:    city.AuthorID,
		CountryName: city.CountryName,
		AuthorName:  city.AuthorName,
		CreatedAt:   fmt.Sprint(city.CreatedAt),
		UpdatedAt:   fmt.Sprint(city.UpdatedAt),
	}
}
