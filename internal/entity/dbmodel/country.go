package dbmodel

import (
	"fmt"
	"time"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
)

type Country struct {
	ID         int64     `db:"id" json:"id,omitempty"`
	Name       string    `db:"name" json:"name,omitempty"`
	ShortName  string    `db:"short_name" json:"short_name,omitempty"`
	Status     int       `db:"status" json:"status,omitempty"`
	AuthorID   int64     `db:"author_id" json:"author_id,omitempty"`
	AuthorName string    `json:"author_name,omitempty"`
	CreatedAt  time.Time `db:"created_at" json:"created_at,omitempty"`
	UpdatedAt  time.Time `db:"updated_at" json:"updated_at,omitempty"`
}

func CountryToProto(country Country) *pb.Country {
	return &pb.Country{
		ID:         country.ID,
		Name:       country.Name,
		ShortName:  country.ShortName,
		Status:     fmt.Sprint(country.Status),
		AuthorId:   country.AuthorID,
		AuthorName: country.AuthorName,
		CreatedAt:  fmt.Sprint(country.CreatedAt),
		UpdatedAt:  fmt.Sprint(country.UpdatedAt),
	}
}
