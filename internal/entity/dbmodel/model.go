package dbmodel

import (
	"fmt"
	"time"

	pb "github.com/akijura/myProject/admin/internal/controller/pb"
)

type Model struct {
	ID         int64     `db:"id" json:"id,omitempty"`
	Name       string    `db:"name" json:"name,omitempty"`
	Status     int       `db:"status" json:"status,omitempty"`
	BrandID    int64     `db:"brand_id" json:"brand_id,omitempty"`
	AuthorID   int64     `db:"author_id" json:"author_id,omitempty"`
	BrandName  string    `json:"brand_name,omitempty"`
	AuthorName string    `json:"author_name,omitempty"`
	CreatedAt  time.Time `db:"created_at" json:"created_at,omitempty"`
	UpdatedAt  time.Time `db:"updated_at" json:"updated_at,omitempty"`
}

func ModelToProto(model Model) *pb.Model {
	return &pb.Model{
		ID:         model.ID,
		Name:       model.Name,
		Status:     fmt.Sprint(model.Status),
		BrandId:    model.BrandID,
		AuthorId:   model.AuthorID,
		BrandName:  model.BrandName,
		AuthorName: model.AuthorName,
		CreatedAt:  fmt.Sprint(model.CreatedAt),
		UpdatedAt:  fmt.Sprint(model.UpdatedAt),
	}
}
