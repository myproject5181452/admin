package domain

type BrandCreateInput struct {
	Name     string `json:"name" binding:"required"`
	Status   int    `json:"status" binding:"required"`
	AuthorID int64  `json:"author_id" binding:"required"`
}

type BrandUpdateInput struct {
	ID       int64  `json:"id" binding:"required"`
	Name     string `json:"name" binding:"required"`
	Status   int    `json:"status" binding:"required"`
	AuthorID int    `json:"author_id" binding:"required"`
}
