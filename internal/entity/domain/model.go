package domain

type ModelCreateInput struct {
	Name     string `json:"name" binding:"required"`
	Status   int    `json:"status" binding:"required"`
	BrandID  int64  `json:"brand_id" binding:"required"`
	AuthorID int64  `json:"author_id" binding:"required"`
}

type ModelUpdateInput struct {
	ID       int64  `json:"id" binding:"required"`
	Name     string `json:"name" binding:"required"`
	BrandID  int64  `json:"brand_id" binding:"required"`
	Status   int    `json:"status" binding:"required"`
	AuthorID int    `json:"author_id" binding:"required"`
}
