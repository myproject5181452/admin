package pgdb

import (
	"context"
	"time"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository/repoerrors"
	"github.com/akijura/myProject/admin/pkg/postgresx"
)

type Brand struct {
	pg *postgresx.Postgres
}

func NewBrand(pg *postgresx.Postgres) *Brand {
	return &Brand{pg: pg}
}

func (c *Brand) GetAllBrands(ctx context.Context) ([]dbmodel.Brand, error) {
	var brands []dbmodel.Brand
	rows, err := c.pg.Pool.Query(ctx, "select cb.id,cb.name,cb.status,cb.author_id,cb.created_at,cb.updated_at, u.name as author_name from car_brands cb left join users u on u.id=cb.author_id where cb.deleted_at is null")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var brand dbmodel.Brand
		err = rows.Scan(
			&brand.ID,
			&brand.Name,
			&brand.Status,
			&brand.AuthorID,
			&brand.CreatedAt,
			&brand.UpdatedAt,
			&brand.AuthorName,
		)
		if err != nil {
			return nil, err
		}
		brands = append(brands, brand)
	}
	return brands, nil
}

func (c *Brand) CreateBrand(ctx context.Context, input domain.BrandCreateInput) error {
	now := time.Now()
	created_at := now.Format(time.RFC3339)
	updated_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `insert into car_brands(name, status,author_id,created_at,updated_at)
	values ($1,$2,$3,$4,$5) returning true`, input.Name, input.Status, input.AuthorID,
		created_at, updated_at)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrCannotCreateData
	}
	return nil
}

func (c *Brand) GetBrandByID(ctx context.Context, ID int64) (dbmodel.Brand, error) {
	var brand dbmodel.Brand
	err := c.pg.Pool.QueryRow(ctx, "select cb.id,cb.name,cb.status,cb.author_id,cb.created_at,cb.updated_at,u.name as author_name from car_brands cb left join users u on u.id=cb.author_id where cb.id = $1 and cb.deleted_at is null", ID).Scan(
		&brand.ID,
		&brand.Name,
		&brand.Status,
		&brand.AuthorID,
		&brand.CreatedAt,
		&brand.UpdatedAt,
		&brand.AuthorName,
	)
	if err != nil {
		if err.Error() == repoerrors.ErrNoRowsResult.Error() {
			return dbmodel.Brand{}, repoerrors.ErrNotFound
		}
		return dbmodel.Brand{}, err
	}
	return brand, nil
}

func (c *Brand) UpdateBrand(ctx context.Context, input domain.BrandUpdateInput) error {
	now := time.Now()
	updated_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `update car_brands set name=$1,status=$2,author_id=$3,updated_at=$4 where id=$5 and deleted_at is null`,
		input.Name,
		input.Status,
		input.AuthorID,
		updated_at,
		input.ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}

func (c *Brand) DeleteBrand(ctx context.Context, ID int64) error {
	now := time.Now()
	deleted_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `update car_brands set deleted_at=$1 where id=$2`,
		deleted_at,
		ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}
