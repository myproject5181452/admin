package pgdb

import (
	"context"
	"time"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository/repoerrors"
	"github.com/akijura/myProject/admin/pkg/postgresx"
)

type CarInfo struct {
	pg *postgresx.Postgres
}

func NewCarInfo(pg *postgresx.Postgres) *CarInfo {
	return &CarInfo{pg: pg}
}

func (c *CarInfo) CreateCarInfo(ctx context.Context, input domain.CarInfoCreateInput) error {
	now := time.Now()
	created_at := now.Format(time.RFC3339)
	updated_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `insert into car_info(model_id, length, width, height, volume, load_capacity, close, author_id, decription, created_at, updated_at)
	values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) returning true`, input.ModelId, input.Length, input.Width,
		input.Height, input.Volume, input.LoadCapacity, input.Close, input.AuthorID, input.Description, created_at, updated_at)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrCannotCreateData
	}
	return nil
}
func (c *CarInfo) GetAllCarInfos(ctx context.Context) ([]dbmodel.CarInfo, error) {
	var carInfos []dbmodel.CarInfo
	rows, err := c.pg.Pool.Query(ctx, "select ci.id,ci.model_id,ci.length,ci.width,ci.height,ci.volume,ci.load_capacity,ci.close,ci.decription,ci.author_id,ci.created_at,ci.updated_at,cm.name as model_name, u.name as author_name from car_info ci left join car_models cm on cm.id=ci.model_id left join users u on u.id=ci.author_id where ci.deleted_at is null")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var carInfo dbmodel.CarInfo
		err = rows.Scan(
			&carInfo.ID,
			&carInfo.ModelId,
			&carInfo.Length,
			&carInfo.Width,
			&carInfo.Height,
			&carInfo.Volume,
			&carInfo.LoadCapacity,
			&carInfo.Close,
			&carInfo.Description,
			&carInfo.AuthorID,
			&carInfo.CreatedAt,
			&carInfo.UpdatedAt,
			&carInfo.ModelName,
			&carInfo.AuthorName,
		)
		if err != nil {
			return nil, err
		}
		carInfos = append(carInfos, carInfo)
	}
	return carInfos, nil
}
func (c *CarInfo) GetCarInfoByID(ctx context.Context, ID int64) (dbmodel.CarInfo, error) {
	var carInfo dbmodel.CarInfo
	err := c.pg.Pool.QueryRow(ctx, "select ci.id,ci.model_id,ci.length,ci.width,ci.height,ci.volume,ci.load_capacity,ci.close,ci.decription,ci.author_id,ci.created_at,ci.updated_at,cm.name as model_name, u.name as author_name from car_info ci left join car_models cm on cm.id=ci.model_id left join users u on u.id=ci.author_id where ci.id=$1 and ci.deleted_at is null", ID).Scan(
		&carInfo.ID,
		&carInfo.ModelId,
		&carInfo.Length,
		&carInfo.Width,
		&carInfo.Height,
		&carInfo.Volume,
		&carInfo.LoadCapacity,
		&carInfo.Close,
		&carInfo.Description,
		&carInfo.AuthorID,
		&carInfo.CreatedAt,
		&carInfo.UpdatedAt,
		&carInfo.ModelName,
		&carInfo.AuthorName,
	)
	if err != nil {
		if err.Error() == repoerrors.ErrNoRowsResult.Error() {
			return dbmodel.CarInfo{}, repoerrors.ErrNotFound
		}
		return dbmodel.CarInfo{}, err
	}
	return carInfo, nil
}
func (c *CarInfo) GetAllCarInfosByModelID(ctx context.Context, modelID int64) ([]dbmodel.CarInfo, error) {
	var carInfos []dbmodel.CarInfo
	rows, err := c.pg.Pool.Query(ctx, "select ci.id,ci.model_id,ci.length,ci.width,ci.height,ci.volume,ci.load_capacity,ci.close,ci.decription,ci.author_id,ci.created_at,ci.updated_at,cm.name as model_name, u.name as author_name from car_info ci left join car_models cm on cm.id=ci.model_id left join users u on u.id=ci.author_id where ci.model_id=$1 and ci.deleted_at is null", modelID)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var carInfo dbmodel.CarInfo
		err = rows.Scan(
			&carInfo.ID,
			&carInfo.ModelId,
			&carInfo.Length,
			&carInfo.Width,
			&carInfo.Height,
			&carInfo.Volume,
			&carInfo.LoadCapacity,
			&carInfo.Close,
			&carInfo.Description,
			&carInfo.AuthorID,
			&carInfo.CreatedAt,
			&carInfo.UpdatedAt,
			&carInfo.ModelName,
			&carInfo.AuthorName,
		)
		if err != nil {
			return nil, err
		}
		carInfos = append(carInfos, carInfo)
	}
	return carInfos, nil
}
func (c *CarInfo) UpdateCarInfo(ctx context.Context, input domain.CarInfoUpdateInput) error {
	now := time.Now()
	updated_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `update car_info set model_id=$1,length=$2,width=$3,height=$4,volume=$5,load_capacity=$6,close=$7,decription=$8,author_id=$9,updated_at=$10 where id=$11 and deleted_at is null`,
		input.ModelID,
		input.Length,
		input.Width,
		input.Height,
		input.Volume,
		input.LoadCapacity,
		input.Close,
		input.Description,
		input.AuthorID,
		updated_at,
		input.ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}
func (c *CarInfo) DeleteCarInfo(ctx context.Context, ID int64) error {
	now := time.Now()
	deleted_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `update car_info set deleted_at=$1 where id=$2`,
		deleted_at,
		ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}
