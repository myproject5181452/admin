package pgdb

import (
	"context"
	"time"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository/repoerrors"
	"github.com/akijura/myProject/admin/pkg/postgresx"
)

type City struct {
	pg *postgresx.Postgres
}

func NewCity(pg *postgresx.Postgres) *City {
	return &City{pg: pg}
}

func (c *City) GetAllCities(ctx context.Context) ([]dbmodel.City, error) {
	var cities []dbmodel.City
	rows, err := c.pg.Pool.Query(ctx, "select ct.id,ct.name,ct.status,ct.country_id,ct.author_id,ct.created_at,ct.updated_at,cy.name as country_name, u.name as author_name from cities ct left join countries cy on cy.id=ct.country_id left join users u on u.id=ct.author_id where ct.deleted_at is null")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var city dbmodel.City
		err = rows.Scan(
			&city.ID,
			&city.Name,
			&city.Status,
			&city.CountryID,
			&city.AuthorID,
			&city.CreatedAt,
			&city.UpdatedAt,
			&city.CountryName,
			&city.AuthorName,
		)
		if err != nil {
			return nil, err
		}
		cities = append(cities, city)
	}
	return cities, nil
}

func (c *City) GetAllCitiesByCountryID(ctx context.Context, countryID int64) ([]dbmodel.City, error) {
	var cities []dbmodel.City
	rows, err := c.pg.Pool.Query(ctx, "select ct.id,ct.name,ct.status,ct.country_id,ct.author_id,ct.created_at,ct.updated_at,cy.name as country_name, u.name as author_name from cities ct left join countries cy on cy.id=ct.country_id left join users u on u.id=ct.author_id where ct.country_id = $1 and ct.deleted_at is null", countryID)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var city dbmodel.City
		err = rows.Scan(
			&city.ID,
			&city.Name,
			&city.Status,
			&city.CountryID,
			&city.AuthorID,
			&city.CreatedAt,
			&city.UpdatedAt,
			&city.CountryName,
			&city.AuthorName,
		)
		if err != nil {
			return nil, err
		}
		cities = append(cities, city)
	}
	return cities, nil
}

func (c *City) CreateCity(ctx context.Context, input domain.CityCreateInput) error {
	now := time.Now()
	created_at := now.Format(time.RFC3339)
	updated_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `insert into cities(name, status, country_id, author_id,created_at,updated_at)
	values ($1,$2,$3,$4,$5,$6) returning true`, input.Name, input.Status, input.CountryID, input.AuthorID,
		created_at, updated_at)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrCannotCreateData
	}
	return nil
}

func (c *City) GetCityByID(ctx context.Context, ID int64) (dbmodel.City, error) {
	var city dbmodel.City
	err := c.pg.Pool.QueryRow(ctx, "select ct.id,ct.name,ct.status,ct.country_id,ct.author_id,ct.created_at,ct.updated_at,cy.name as country_name, u.name as author_name from cities ct left join countries cy on cy.id=ct.country_id left join users u on u.id=ct.author_id where ct.id = $1 and ct.deleted_at is null", ID).Scan(
		&city.ID,
		&city.Name,
		&city.Status,
		&city.CountryID,
		&city.AuthorID,
		&city.CreatedAt,
		&city.UpdatedAt,
		&city.CountryName,
		&city.AuthorName,
	)
	if err != nil {
		if err.Error() == repoerrors.ErrNoRowsResult.Error() {
			return dbmodel.City{}, repoerrors.ErrNotFound
		}
		return dbmodel.City{}, err
	}
	return city, nil
}

func (c *City) UpdateCity(ctx context.Context, input domain.CityUpdateInput) error {
	now := time.Now()
	updated_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `update cities set name=$1,status=$2,country_id=$3,author_id=$4,updated_at=$5 where id=$6 and deleted_at is null`,
		input.Name,
		input.Status,
		input.CountryID,
		input.AuthorID,
		updated_at,
		input.ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}

func (c *City) DeleteCity(ctx context.Context, ID int64) error {
	now := time.Now()
	deleted_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `update cities set deleted_at=$1 where id=$2`,
		deleted_at,
		ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}
