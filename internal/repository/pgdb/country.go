package pgdb

import (
	"context"
	"time"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository/repoerrors"
	"github.com/akijura/myProject/admin/pkg/postgresx"
)

type Country struct {
	pg *postgresx.Postgres
}

func NewCountry(pg *postgresx.Postgres) *Country {
	return &Country{pg: pg}
}

func (c *Country) GetCountryByID(ctx context.Context, ID int64) (dbmodel.Country, error) {
	var country dbmodel.Country
	err := c.pg.Pool.QueryRow(ctx, "select ct.id,ct.name,ct.short_name,ct.status,ct.author_id,ct.created_at,ct.updated_at,u.name as author_name from countries ct left join users u on u.id=ct.author_id where ct.id = $1 and ct.deleted_at is null", ID).Scan(
		&country.ID,
		&country.Name,
		&country.ShortName,
		&country.Status,
		&country.AuthorID,
		&country.CreatedAt,
		&country.UpdatedAt,
		&country.AuthorName,
	)
	if err != nil {
		if err.Error() == repoerrors.ErrNoRowsResult.Error() {
			return dbmodel.Country{}, repoerrors.ErrNotFound
		}
		return dbmodel.Country{}, err
	}
	return country, nil
}

func (c *Country) GetAllCountry(ctx context.Context) ([]dbmodel.Country, error) {
	var countries []dbmodel.Country
	rows, err := c.pg.Pool.Query(ctx, "select ct.id,ct.name,ct.short_name,ct.status,ct.author_id,ct.created_at,ct.updated_at,u.name as author_name from countries ct left join users u on u.id=ct.author_id where ct.deleted_at is null")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var country dbmodel.Country
		err = rows.Scan(
			&country.ID,
			&country.Name,
			&country.ShortName,
			&country.Status,
			&country.AuthorID,
			&country.CreatedAt,
			&country.UpdatedAt,
			&country.AuthorName,
		)
		if err != nil {
			return nil, err
		}
		countries = append(countries, country)
	}
	return countries, nil
}

func (c *Country) CreateCountry(ctx context.Context, input domain.CountryCreateInput) error {
	now := time.Now()
	created_at := now.Format(time.RFC3339)
	updated_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `insert into countries(name, short_name, status, author_id,created_at,updated_at)
	values ($1,$2,$3,$4,$5,$6) returning true`, input.Name, input.ShortName, input.Status, input.AuthorID,
		created_at, updated_at)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrCannotCreateData
	}
	return nil
}

func (c *Country) UpdateCountry(ctx context.Context, input domain.CountryUpdateInput) error {
	now := time.Now()
	updated_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `update countries set name=$1,short_name=$2,status=$3,author_id=$4,updated_at=$5 where id=$6 and deleted_at is null`,
		input.Name,
		input.ShortName,
		input.Status,
		input.AuthorID,
		updated_at,
		input.ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}

func (c *Country) DeleteCountry(ctx context.Context, ID int64) error {
	now := time.Now()
	deleted_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `update countries set deleted_at=$1 where id=$2`,
		deleted_at,
		ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}
