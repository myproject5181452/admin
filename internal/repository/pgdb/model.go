package pgdb

import (
	"context"
	"time"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository/repoerrors"
	"github.com/akijura/myProject/admin/pkg/postgresx"
)

type Model struct {
	pg *postgresx.Postgres
}

func NewModel(pg *postgresx.Postgres) *Model {
	return &Model{pg: pg}
}
func (m *Model) GetAllModels(ctx context.Context) ([]dbmodel.Model, error) {
	var models []dbmodel.Model
	rows, err := m.pg.Pool.Query(ctx, "select cm.id,cm.name,cm.status,cm.brand_id,cm.author_id,cm.created_at,cm.updated_at,cb.name as brand_name, u.name as author_name from car_models cm left join car_brands cb on cb.id=cm.brand_id left join users u on u.id=cm.author_id where cm.deleted_at is null")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var model dbmodel.Model
		err = rows.Scan(
			&model.ID,
			&model.Name,
			&model.Status,
			&model.BrandID,
			&model.AuthorID,
			&model.CreatedAt,
			&model.UpdatedAt,
			&model.BrandName,
			&model.AuthorName,
		)
		if err != nil {
			return nil, err
		}
		models = append(models, model)
	}
	return models, nil
}

func (m *Model) GetAllModelsByBrandID(ctx context.Context, brandID int64) ([]dbmodel.Model, error) {
	var models []dbmodel.Model
	rows, err := m.pg.Pool.Query(ctx, "select cm.id,cm.name,cm.status,cm.brand_id,cm.author_id,cm.created_at,cm.updated_at,cb.name as brand_name, u.name as author_name from car_models cm left join car_brands cb on cb.id=cm.brand_id left join users u on u.id=cm.author_id where cm.brand_id = $1 and cm.deleted_at is null", brandID)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var model dbmodel.Model
		err = rows.Scan(
			&model.ID,
			&model.Name,
			&model.Status,
			&model.BrandID,
			&model.AuthorID,
			&model.CreatedAt,
			&model.UpdatedAt,
			&model.BrandName,
			&model.AuthorName,
		)
		if err != nil {
			return nil, err
		}
		models = append(models, model)
	}
	return models, nil
}

func (m *Model) CreateModel(ctx context.Context, input domain.ModelCreateInput) error {
	now := time.Now()
	created_at := now.Format(time.RFC3339)
	updated_at := now.Format(time.RFC3339)
	res, err := m.pg.Pool.Exec(ctx, `insert into car_models(name, status, brand_id, author_id,created_at,updated_at)
	values ($1,$2,$3,$4,$5,$6) returning true`, input.Name, input.Status, input.BrandID, input.AuthorID,
		created_at, updated_at)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrCannotCreateData
	}
	return nil
}

func (m *Model) GetModelByID(ctx context.Context, ID int64) (dbmodel.Model, error) {
	var model dbmodel.Model
	err := m.pg.Pool.QueryRow(ctx, "select cm.id,cm.name,cm.status,cm.brand_id,cm.author_id,cm.created_at,cm.updated_at,cb.name as brand_name, u.name as author_name from car_models cm left join car_brands cb on cb.id=cm.brand_id left join users u on u.id=cm.author_id where cm.id = $1 and cm.deleted_at is null", ID).Scan(
		&model.ID,
		&model.Name,
		&model.Status,
		&model.BrandID,
		&model.AuthorID,
		&model.CreatedAt,
		&model.UpdatedAt,
		&model.BrandName,
		&model.AuthorName,
	)
	if err != nil {
		if err.Error() == repoerrors.ErrNoRowsResult.Error() {
			return dbmodel.Model{}, repoerrors.ErrNotFound
		}
		return dbmodel.Model{}, err
	}
	return model, nil
}

func (m *Model) UpdateModel(ctx context.Context, input domain.ModelUpdateInput) error {
	now := time.Now()
	updated_at := now.Format(time.RFC3339)
	res, err := m.pg.Pool.Exec(ctx, `update car_models set name=$1,status=$2,brand_id=$3,author_id=$4,updated_at=$5 where id=$6 and deleted_at is null`,
		input.Name,
		input.Status,
		input.BrandID,
		input.AuthorID,
		updated_at,
		input.ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}

func (m *Model) DeleteModel(ctx context.Context, ID int64) error {
	now := time.Now()
	deleted_at := now.Format(time.RFC3339)
	res, err := m.pg.Pool.Exec(ctx, `update car_models set deleted_at=$1 where id=$2`,
		deleted_at,
		ID,
	)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrNotFound
	}
	return nil
}
