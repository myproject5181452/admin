package pgdb

import (
	"context"
	"time"

	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository/repoerrors"
	"github.com/akijura/myProject/admin/pkg/postgresx"
)

type User struct {
	pg *postgresx.Postgres
}

func NewUser(pg *postgresx.Postgres) *User {
	return &User{pg: pg}
}

func (c *User) CreateUser(ctx context.Context, input domain.UserCreateInput) error {
	now := time.Now()
	created_at := now.Format(time.RFC3339)
	updated_at := now.Format(time.RFC3339)
	res, err := c.pg.Pool.Exec(ctx, `insert into users(login, password, name,phone_number,role,status,created_at,updated_at)
	values ($1,$2,$3,$4,$5,$6,$7,$8)`, input.Login, input.Password, input.Name, input.PhoneNumber, input.Role, input.Status,
		created_at, updated_at)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return repoerrors.ErrCannotCreateData
	}
	return nil
}
