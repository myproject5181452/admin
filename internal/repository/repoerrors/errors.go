package repoerrors

import "errors"

var (
	ErrNotFound         = errors.New("data not found or passive")
	ErrCannotUpdateData = errors.New("failed  update record")
	ErrCannotCreateData = errors.New("failed create record")
	ErrCannotDeleteData = errors.New("failed delete data(s)")
	ErrNoAdminRights    = errors.New("permission denied")
	ErrTokenExpired     = errors.New("token expired")
	ErrNoRowsResult     = errors.New("no rows in result set")
)
