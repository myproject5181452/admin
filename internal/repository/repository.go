package repository

import (
	"context"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository/pgdb"
	"github.com/akijura/myProject/admin/internal/repository/upload"
	"github.com/akijura/myProject/admin/pkg/postgresx"
)

type Country interface {
	GetCountryByID(ctx context.Context, ID int64) (dbmodel.Country, error)
	GetAllCountry(ctx context.Context) ([]dbmodel.Country, error)
	CreateCountry(ctx context.Context, input domain.CountryCreateInput) error
	UpdateCountry(ctx context.Context, input domain.CountryUpdateInput) error
	DeleteCountry(ctx context.Context, ID int64) error
}

type City interface {
	GetAllCities(ctx context.Context) ([]dbmodel.City, error)
	CreateCity(ctx context.Context, input domain.CityCreateInput) error
	GetCityByID(ctx context.Context, ID int64) (dbmodel.City, error)
	UpdateCity(ctx context.Context, input domain.CityUpdateInput) error
	DeleteCity(ctx context.Context, ID int64) error
	GetAllCitiesByCountryID(ctx context.Context, ID int64) ([]dbmodel.City, error)
}

type Brand interface {
	GetAllBrands(ctx context.Context) ([]dbmodel.Brand, error)
	CreateBrand(ctx context.Context, input domain.BrandCreateInput) error
	GetBrandByID(ctx context.Context, ID int64) (dbmodel.Brand, error)
	UpdateBrand(ctx context.Context, input domain.BrandUpdateInput) error
	DeleteBrand(ctx context.Context, ID int64) error
}

type User interface {
	CreateUser(ctx context.Context, input domain.UserCreateInput) error
}

type File interface {
	SetFile(fileName, path string) error
	Write(chunk []byte) error
	Close() error
}
type CarInfo interface {
	CreateCarInfo(ctx context.Context, input domain.CarInfoCreateInput) error
	GetAllCarInfos(ctx context.Context) ([]dbmodel.CarInfo, error)
	GetCarInfoByID(ctx context.Context, ID int64) (dbmodel.CarInfo, error)
	GetAllCarInfosByModelID(ctx context.Context, modelID int64) ([]dbmodel.CarInfo, error)
	UpdateCarInfo(ctx context.Context, input domain.CarInfoUpdateInput) error
	DeleteCarInfo(ctx context.Context, ID int64) error
}
type Model interface {
	GetAllModels(ctx context.Context) ([]dbmodel.Model, error)
	CreateModel(ctx context.Context, input domain.ModelCreateInput) error
	GetModelByID(ctx context.Context, ID int64) (dbmodel.Model, error)
	UpdateModel(ctx context.Context, input domain.ModelUpdateInput) error
	DeleteModel(ctx context.Context, ID int64) error
	GetAllModelsByBrandID(ctx context.Context, brandID int64) ([]dbmodel.Model, error)
}
type Repositories struct {
	Country
	User
	City
	Brand
	Model
	File
	CarInfo
}

func NewRepositories(pg *postgresx.Postgres) *Repositories {
	return &Repositories{
		pgdb.NewCountry(pg),
		pgdb.NewUser(pg),
		pgdb.NewCity(pg),
		pgdb.NewBrand(pg),
		pgdb.NewModel(pg),
		upload.NewFile(),
		pgdb.NewCarInfo(pg),
	}
}
