package service

import (
	"context"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository"
)

type BrandService struct {
	repo *repository.Repositories
}

func NewBrandService(repo *repository.Repositories) *BrandService {
	return &BrandService{repo: repo}
}

func (c *BrandService) GetBrandByID(ctx context.Context, ID int64) (dbmodel.Brand, error) {
	res, err := c.repo.Brand.GetBrandByID(ctx, ID)
	return res, err
}

func (c *BrandService) GetAllBrands(ctx context.Context) ([]dbmodel.Brand, error) {
	res, err := c.repo.Brand.GetAllBrands(ctx)
	return res, err
}

func (d *BrandService) CreateBrand(ctx context.Context, input domain.BrandCreateInput) error {
	err := d.repo.Brand.CreateBrand(ctx, input)
	return err
}

func (d *BrandService) UpdateBrand(ctx context.Context, input domain.BrandUpdateInput) error {
	err := d.repo.Brand.UpdateBrand(ctx, input)
	return err
}

func (c *BrandService) DeleteBrand(ctx context.Context, ID int64) error {
	err := c.repo.Brand.DeleteBrand(ctx, ID)
	return err
}
