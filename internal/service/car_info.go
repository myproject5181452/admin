package service

import (
	"context"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository"
)

type CarInfoService struct {
	repo *repository.Repositories
}

func NewCarInfoService(repo *repository.Repositories) *CarInfoService {
	return &CarInfoService{repo: repo}
}

func (c *CarInfoService) CreateCarInfo(ctx context.Context, input domain.CarInfoCreateInput) error {
	err := c.repo.CarInfo.CreateCarInfo(ctx, input)
	return err
}
func (c *CarInfoService) GetAllCarInfos(ctx context.Context) ([]dbmodel.CarInfo, error) {
	res, err := c.repo.CarInfo.GetAllCarInfos(ctx)
	return res, err
}
func (c *CarInfoService) GetCarInfoByID(ctx context.Context, ID int64) (dbmodel.CarInfo, error) {
	res, err := c.repo.CarInfo.GetCarInfoByID(ctx, ID)
	return res, err
}

func (c *CarInfoService) GetAllCarInfosByModelID(ctx context.Context, modelID int64) ([]dbmodel.CarInfo, error) {
	res, err := c.repo.CarInfo.GetAllCarInfosByModelID(ctx, modelID)
	return res, err
}
func (c *CarInfoService) UpdateCarInfo(ctx context.Context, input domain.CarInfoUpdateInput) error {
	err := c.repo.CarInfo.UpdateCarInfo(ctx, input)
	return err
}
func (c *CarInfoService) DeleteCarInfo(ctx context.Context, ID int64) error {
	err := c.repo.CarInfo.DeleteCarInfo(ctx, ID)
	return err
}
