package service

import (
	"context"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository"
)

type CityService struct {
	repo *repository.Repositories
}

func NewCityService(repo *repository.Repositories) *CityService {
	return &CityService{repo: repo}
}

func (c *CityService) GetCityByID(ctx context.Context, ID int64) (dbmodel.City, error) {
	res, err := c.repo.City.GetCityByID(ctx, ID)
	return res, err
}

func (c *CityService) GetAllCities(ctx context.Context) ([]dbmodel.City, error) {
	res, err := c.repo.City.GetAllCities(ctx)
	return res, err
}

func (d *CityService) CreateCity(ctx context.Context, input domain.CityCreateInput) error {
	err := d.repo.City.CreateCity(ctx, input)
	return err
}

func (d *CityService) UpdateCity(ctx context.Context, input domain.CityUpdateInput) error {
	err := d.repo.City.UpdateCity(ctx, input)
	return err
}

func (c *CityService) DeleteCity(ctx context.Context, ID int64) error {
	err := c.repo.City.DeleteCity(ctx, ID)
	return err
}

func (c *CityService) GetAllCitiesByCountryID(ctx context.Context, ID int64) ([]dbmodel.City, error) {
	res, err := c.repo.City.GetAllCitiesByCountryID(ctx, ID)
	return res, err
}
