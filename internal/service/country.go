package service

import (
	"context"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository"
)

type CountryService struct {
	repo *repository.Repositories
}

func NewCountryService(repo *repository.Repositories) *CountryService {
	return &CountryService{repo: repo}
}

func (c *CountryService) GetCountryByID(ctx context.Context, ID int64) (dbmodel.Country, error) {
	res, err := c.repo.Country.GetCountryByID(ctx, ID)
	return res, err
}

func (c *CountryService) GetAllCountry(ctx context.Context) ([]dbmodel.Country, error) {
	res, err := c.repo.Country.GetAllCountry(ctx)
	return res, err
}

func (d *CountryService) CreateCountry(ctx context.Context, input domain.CountryCreateInput) error {
	err := d.repo.Country.CreateCountry(ctx, input)
	return err
}

func (d *CountryService) UpdateCountry(ctx context.Context, input domain.CountryUpdateInput) error {
	err := d.repo.Country.UpdateCountry(ctx, input)
	return err
}

func (c *CountryService) DeleteCountry(ctx context.Context, ID int64) error {
	err := c.repo.Country.DeleteCountry(ctx, ID)
	return err
}
