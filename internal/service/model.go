package service

import (
	"context"

	"github.com/akijura/myProject/admin/internal/entity/dbmodel"
	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository"
)

type ModelService struct {
	repo *repository.Repositories
}

func NewModelService(repo *repository.Repositories) *ModelService {
	return &ModelService{repo: repo}
}

func (c *ModelService) GetModelByID(ctx context.Context, ID int64) (dbmodel.Model, error) {
	res, err := c.repo.Model.GetModelByID(ctx, ID)
	return res, err
}

func (c *ModelService) GetAllModels(ctx context.Context) ([]dbmodel.Model, error) {
	res, err := c.repo.Model.GetAllModels(ctx)
	return res, err
}

func (d *ModelService) CreateModel(ctx context.Context, input domain.ModelCreateInput) error {
	err := d.repo.Model.CreateModel(ctx, input)
	return err
}

func (d *ModelService) UpdateModel(ctx context.Context, input domain.ModelUpdateInput) error {
	err := d.repo.Model.UpdateModel(ctx, input)
	return err
}

func (c *ModelService) DeleteModel(ctx context.Context, ID int64) error {
	err := c.repo.Model.DeleteModel(ctx, ID)
	return err
}

func (c *ModelService) GetAllModelsByBrandID(ctx context.Context, ID int64) ([]dbmodel.Model, error) {
	res, err := c.repo.Model.GetAllModelsByBrandID(ctx, ID)
	return res, err
}
