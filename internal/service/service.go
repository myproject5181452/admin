package service

import (
	"github.com/akijura/myProject/admin/internal/config"
	"github.com/akijura/myProject/admin/internal/repository"
)

type Service struct {
	repo *repository.Repositories
	conf *config.Config

	Country     *CountryService
	User        *UserService
	City        *CityService
	Brand       *BrandService
	Model       *ModelService
	FileService *FileService
	CarInfo     *CarInfoService
}

func NewService(repo *repository.Repositories, conf *config.Config) *Service {
	return &Service{
		repo: repo,
		conf: conf,

		Country:     NewCountryService(repo),
		User:        NewUserService(repo),
		City:        NewCityService(repo),
		Brand:       NewBrandService(repo),
		Model:       NewModelService(repo),
		FileService: NewFileService(repo),
		CarInfo:     NewCarInfoService(repo),
	}
}
