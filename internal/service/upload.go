package service

import (
	"github.com/akijura/myProject/admin/internal/repository"
)

type FileService struct {
	repo *repository.Repositories
}

func NewFileService(repo *repository.Repositories) *FileService {
	return &FileService{repo: repo}
}

func (c *FileService) SetFile(fileName, path string) error {
	err := c.repo.File.SetFile(fileName, path)
	return err
}

func (c *FileService) Write(chunk []byte) error {
	err := c.repo.File.Write(chunk)
	return err
}
func (c *FileService) Close() error {
	err := c.repo.File.Close()
	return err
}
