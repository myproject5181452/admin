package service

import (
	"context"

	"github.com/akijura/myProject/admin/internal/entity/domain"
	"github.com/akijura/myProject/admin/internal/repository"
)

type UserService struct {
	repo *repository.Repositories
}

func NewUserService(repo *repository.Repositories) *UserService {
	return &UserService{repo: repo}
}

func (d *UserService) CreateUser(ctx context.Context, input domain.UserCreateInput) error {
	err := d.repo.User.CreateUser(ctx, input)
	return err
}
