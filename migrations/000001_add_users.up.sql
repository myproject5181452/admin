CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "login" varchar(50) UNIQUE NOT NULL,
  "password" varchar(255) NOT NULL,
  "name" varchar(255) NOT NULL,
  "phone_number" varchar(50),
  "role" int NOT NULL,
  "status" int NOT NULL DEFAULT 1, 
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now())
);
