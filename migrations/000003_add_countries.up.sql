CREATE TABLE "countries" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar(100) UNIQUE NOT NULL,
  "short_name" varchar(10) UNIQUE NOT NULL,
  "status" int NOT NULL DEFAULT 1,
  "author_id" int NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now()),
  "deleted_at" timestamp null,
  CONSTRAINT "fk_user_id" FOREIGN KEY("author_id") REFERENCES "users"("id")
);