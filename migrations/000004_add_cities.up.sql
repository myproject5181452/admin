CREATE TABLE "cities" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar(100) UNIQUE NOT NULL,
  "status" int NOT NULL DEFAULT 1,
  "country_id" int NOT NULL,
  "author_id" int NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now()),
  "deleted_at" timestamp null,
   CONSTRAINT "fk_country_id" FOREIGN KEY("country_id") REFERENCES "countries"("id"),
   CONSTRAINT "fk_user_id" FOREIGN KEY("author_id") REFERENCES "users"("id")
);