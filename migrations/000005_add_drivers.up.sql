CREATE TABLE "drivers" (
  "id" SERIAL PRIMARY KEY,
  "phone_number" varchar(50) UNIQUE NOT NULL,
  "first_name" varchar(255),
  "last_name" varchar(255),
  "city_id" int,
  "status" int NOT NULL DEFAULT 1,
  "is_approved" int NOT NULL DEFAULT 0,
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now()),
  "deleted_at" timestamp null,

  CONSTRAINT "fk_city_id" FOREIGN KEY("city_id") REFERENCES "cities"("id")
);
