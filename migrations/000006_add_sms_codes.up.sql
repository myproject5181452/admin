CREATE TABLE "sms_codes" (
  "id" SERIAL PRIMARY KEY,
  "phone_number" varchar(50) UNIQUE NOT NULL,
  "ver_code" int NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now())
);
