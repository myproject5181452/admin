CREATE TABLE "car_models" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar(50) UNIQUE NOT NULL,
  "status" int NOT NULL DEFAULT 1,
  "author_id" int NOT NULL,
  "brand_id" int NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now()),
  "deleted_at" timestamp null,
  CONSTRAINT "fk_brand_id" FOREIGN KEY("brand_id") REFERENCES "car_brands"("id"),
  CONSTRAINT "fk_user_id" FOREIGN KEY("author_id") REFERENCES "users"("id")
);