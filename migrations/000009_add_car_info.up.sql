CREATE TABLE "car_info" (
  "id" SERIAL PRIMARY KEY,
  "model_id" int UNIQUE NOT NULL,
  "length" int NOT NULL,
  "width" int NOT NULL,
  "height" int NOT NULL,
  "volume" int NOT NULL,
  "load_capacity" int NOT NULL,
  "close" int NOT NULL DEFAULT 1,
  "author_id" int NOT NULL,
  "decription" TEXT NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now()),
  "deleted_at" timestamp null,
  CONSTRAINT "fk_model_id" FOREIGN KEY("model_id") REFERENCES "car_models"("id"),
  CONSTRAINT "fk_user_id" FOREIGN KEY("author_id") REFERENCES "users"("id")
);