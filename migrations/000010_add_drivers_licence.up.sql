CREATE TABLE "drivers_license" (
  "id" SERIAL PRIMARY KEY,
  "driver_id" int UNIQUE NOT NULL,
  "country_id" int NOT NULL,
  "number_license" varchar(50) UNIQUE NOT NULL,
  "given_date" DATE NOT NULL DEFAULT CURRENT_DATE,
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now()),
  "deleted_at" timestamp null,
  CONSTRAINT "fk_country_id" FOREIGN KEY("country_id") REFERENCES "countries"("id"),
  CONSTRAINT "fk_driver_id" FOREIGN KEY("driver_id") REFERENCES "drivers"("id")
);