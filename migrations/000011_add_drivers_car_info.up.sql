CREATE TABLE "drivers_car_info" (
  "id" SERIAL PRIMARY KEY,
  "driver_id" int NOT NULL,
  "model_id" int NOT NULL,
  "color" int NOT NULL,
  "car_number" varchar(50) UNIQUE NOT NULL,
  "car_tex_number" varchar(50) UNIQUE NOT NULL,
  "car_date" DATE NOT NULL DEFAULT CURRENT_DATE,
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now()),
  "deleted_at" timestamp null,
  CONSTRAINT "fk_model_id" FOREIGN KEY("model_id") REFERENCES "car_models"("id"),
  CONSTRAINT "fk_driver_id" FOREIGN KEY("driver_id") REFERENCES "drivers"("id")
);