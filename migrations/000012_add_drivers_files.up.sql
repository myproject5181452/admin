CREATE TABLE "drivers_files" (
  "id" SERIAL PRIMARY KEY,
  "driver_id" int NOT NULL,
  "license_photo" varchar(250),
  "license_photo_name" varchar(250) UNIQUE,
  "car_photo" varchar(250),
  "car_photo_name" varchar(250) UNIQUE,
  "tex_photo" varchar(250),
  "tex_photo_name" varchar(250) UNIQUE,
  "created_at" timestamptz NOT NULL DEFAULT (now()),
  "updated_at" timestamptz NOT NULL DEFAULT (now()),
  "deleted_at" timestamp null,
  CONSTRAINT "fk_driver_id" FOREIGN KEY("driver_id") REFERENCES "drivers"("id")
);